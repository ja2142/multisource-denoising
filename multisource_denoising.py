#!/usr/bin/env python3

# there was some cool article about improving signal to noise ratio by measuring signal with many sensors (e.g. microphones)
# that works, provided all sensors measure the same thing(without latencies at the order of wavelength, probably?),
# and noise in sensors is not correlated
# if both of these conditions are fullfiled, signal rises linearly with number of sensors, but noise doesn't (because noise
# from one sensor is equally likely to be in phase and out of phase with respect to other sensors)
# below is theoretical presentation of that

import math
import random
import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft

# number of times sine repeats in measurement window (which is also index of that frequency in resulting fft)
WAVE_REPEATS = 10

def gen_noise(sample_count=4096, level=1):
    return np.array([level*random.uniform(-1, 1) for _ in range(sample_count)])

def gen_wave(sample_count=4096, level=1):
    phi_inc = WAVE_REPEATS/sample_count
    return np.array([level * math.cos(2 * math.pi * (i * phi_inc)) for i in range(sample_count)])

def gen_wave_noisy(sample_count=4096, snr=1):
    return gen_wave(sample_count) + gen_noise(sample_count, 1/snr)

def ratio_to_db(ratio):
    return 20 * math.log(ratio) / math.log(10)

def format_ratio(ratio):
    return f'{ratio:.3} ({ratio_to_db(ratio):.3} dB)'

def calc_noise_lvl(freqs):
    freqs = np.abs(freqs)
    INTEREST_FREQ = WAVE_REPEATS
    return np.average(freqs[INTEREST_FREQ:])

def calc_signal_lvl(freqs):
    freqs = np.abs(freqs)
    INTEREST_FREQ = WAVE_REPEATS
    return freqs[INTEREST_FREQ]

def calc_snr(freqs):
    return calc_signal_lvl(freqs) / calc_noise_lvl(freqs)

def plot_n_sources(rows_total, this_row, source_count=1):
    COLUMNS = 2
    this_index = this_row * COLUMNS + 1

    samples = sum([gen_wave_noisy() for i in range(source_count)])

    freqs = fft(samples)

    samples_plot = plt.subplot(rows_total, COLUMNS, this_index)
    plt.plot(np.arange(len(samples)), samples, 'r')
    samples_plot.set_xlabel(f'samples from {source_count} sources')

    fft_plot = plt.subplot(rows_total, COLUMNS, this_index + 1)
    plt.stem(np.arange(len(freqs)), np.abs(freqs), 'b', markerfmt=" ", basefmt="-b")
    fft_plot.set_xlabel(f'fft of samples from {source_count} sources')

    print(f'{source_count} sources snr: {format_ratio(calc_snr(freqs))}')

def plot_source_cnt_vs_lvl(max_src_count=128):
    counts = np.arange(1, max_src_count + 1)
    # signal level raises linearly (signal+signal=2*signal)
    signal_lvls = counts
    noise_lvls = []

    noise = gen_noise()
    for i in range(1, max_src_count + 1):
        noise += gen_noise()
        noise_lvls.append(calc_noise_lvl(noise))

    snrs = signal_lvls / noise_lvls

    plt.figure(figsize = (12, 6))

    plot = plt.subplot(1, 2, 1)
    plt.title('signal, noise, snr vs source count')
    plot.set_ylabel('level')
    plot.set_xlabel('source count')
    plt.plot(counts, signal_lvls, label='signal levels')
    plt.plot(counts, noise_lvls, label='noise level')
    plt.plot(counts, snrs, label='signal to noise ratio')
    plt.legend()

    plot = plt.subplot(1, 2, 2)
    plt.title('signal, noise, snr vs source count (log-log)')
    plot.set_ylabel('level')
    plot.set_xlabel('source count')
    plt.loglog(counts, signal_lvls, label='signal levels')
    plt.loglog(counts, noise_lvls, label='noise level')
    plt.loglog(counts, snrs, label='signal to noise ratio')
    plt.legend()

    plt.show()

def main():
    plt.figure(figsize = (12, 9))

    plot_n_sources(6, 0, source_count=1)
    plot_n_sources(6, 1, source_count=2)
    plot_n_sources(6, 2, source_count=4)
    plot_n_sources(6, 3, source_count=8)
    plot_n_sources(6, 4, source_count=16)
    plot_n_sources(6, 5, source_count=32)

    plt.tight_layout()
    plt.show()

    plot_source_cnt_vs_lvl()

if __name__ == '__main__':
    main()

